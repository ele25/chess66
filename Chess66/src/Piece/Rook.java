package Piece;

import Chess66.chess;

/**
 * Rook class extends Piece
 * @author edward esposito and Marjan Atienza
 * 
 */
public class Rook extends Piece {

	/**
	 * boolean to tell if rook has moved. Used for castling 
	 */
	public boolean hasMoved = false;
	
	/**
	 * Constructor for Rook class
	 *  @param value Rook value 
	 * 	@param color Rook color
	 * 
	 */
	public Rook(String value, int color) {
		super(value, color);
	}
	
	/**
	 * Checks for legal move for Rook
	 * @param curr Rook current position
	 * @param newpos new position Rook will move to 
	 * @param color Rook's color 
	 * @param enpass specifically for pawn, ignore by other pieces
	 * @return boolean if move is legal for Rook
	 */
	public boolean LegalMove(String curr, String newpos, int color, boolean enpass) {
		int currRow = curr.charAt(1) - '0';
		int newRow = newpos.charAt(1) - '0';
		char currCol = curr.charAt(0);
		char newCol = newpos.charAt(0);
		
		if((currRow == newRow) || (currCol == newCol)) {
			if(!(clearPath(curr,newpos))) {
				//System.out.println("here");
				return false;
			}
				if(isOccupied(newpos) == true) {
					Piece newspot = chess.board.get(newpos);
					
					// same color piece so capture not permitted
					if(newspot.getColor() == color) {
						//System.out.println("here1");
						return false;
					}
				}
				// Rook has moved so now cannot be used for castling
				this.hasMoved = true;
				return true;
			
		}
		//System.out.println("here4");
		return false; 
	
	}

	/** Rook's clear path.
	 * @param curr Queen Current position 
	 * @param newpos position Queen is moving 
	 * @return if lateral and vertical moves are clear 
	 */
public boolean clearPath(String curr, String newpos) {
	int i;
	int currRow = curr.charAt(1) - '0';
	int newRow = newpos.charAt(1) - '0';
	char currCol = curr.charAt(0);
	char newCol = newpos.charAt(0);
	
	if (currRow == newRow) { //Moving in row (horizontal)
		char letter;
		if(currCol < newCol) { 
			for (letter = (char)(currCol+1) ; letter < newCol ; letter++) {
				if(!(isClearHelper(letter, currRow))) {
					return false;
				}
			}
		}
		else { 
			for (letter = (char)(newCol+1) ; letter < currCol ; letter++) {
				if(!(isClearHelper(letter, currRow))) {
					return false;
				}
			}
		}
	}else if(currCol == newCol) { //Moving in column (vertical)
		if(currRow < newRow) { //Moving up 
			for (i = currRow+1 ; i < newRow ; i++) {
				//System.out.println("1: "+ i);
				if(!(isClearHelper(currCol, i))) {
					//System.out.println("1: problem "+ i);
				return false;
				}
			}
		} else {  //Moving Down 
			for (i = newRow+1 ; i < currRow ; i++) {
				if(!(isClearHelper(currCol, i))) {
					//System.out.println("2: "+ i);
				return false;
				}
			}
		}
	}
		
		return true;
	}

/** 
 * Helper for  clear path 
 * @param o char for first part of position 
 * @param i int for second part of position 
 * @return if spot is clear 
 */
	public boolean isClearHelper(char o, int i) {
		String str = o + "" + i;
		
		if(!isOccupied(str)) { 
			//System.out.println("Box is empty");
			return true;
		}
		
		return false;
	}
}
