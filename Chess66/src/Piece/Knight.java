package Piece;

import Chess66.chess;

/**
 * Knight class extends Piece 
 * @author edward esposito and Marjan Atienza
 *
 */
public class Knight extends Piece {

	/**
	 * Constructor for Knight class
	 * @param value value of knight 
	 * @param color color of Knight 
	 */
	public Knight(String value, int color) {
		super(value, color);
	}
	
	/**Since the Knight can jump over other pieces, its clear path is true
	 * @param curr knight current position
	 * @param newpos position knight will move to 
	 * @return true 
	 * 
	 */
	public boolean clearPath(String curr, String newpos) {
		return true;
	}
	
	/**Checks for legal move of Knight. 
	 * @param curr Knight's current position before moving
	 * @param newpos position that Knight is moving to 
	 * @param  color color of the Knight
	 * @param enpass specificly for pawn, check if enpass is valid, ignored by other pieces 
	 * @return if the move is legal 
	 */
	public boolean LegalMove(String curr, String newpos, int color, boolean enpass) {
		int currRow = curr.charAt(1) - '0';
		int newRow = newpos.charAt(1) - '0';
		char currCol = curr.charAt(0);
		char newCol = newpos.charAt(0);
		int deltaX;
		int deltaY;
		
		deltaX = Math.abs(newRow-currRow);
		deltaY = Math.abs(newCol-currCol);
		
		if(!(isOccupied(newpos))) {

		if (deltaX == 2 && deltaY == 1){
			return true;
		} else if (deltaX == 1 && deltaY == 2) {
			return true;
		}
	}	
		
		if(isOccupied(newpos)) {
			Piece newspot = chess.board.get(newpos);
			if(newspot.getColor()==color) {
				return false;
			}else if(deltaX == 2 && deltaY == 1){
				return true;
			} else if (deltaX == 1 && deltaY == 2) {
				return true;
			}
		}
		return false;
	}
		
		
		/*if(isOccupied(newpos)) {
			Piece newspot = chess.board.get(newpos);
			if(newspot.getColor()==color) {
				return false;
			}else {
				return true;
			}
		}
		
		return false;
	}*/

}
