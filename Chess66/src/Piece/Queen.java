package Piece;

import java.util.ArrayList;
import java.util.List;

import Chess66.chess;

/**
 * Queen class extends Piece
 * @author edward esposito and Marjan Atienza
 *
 */
public class Queen extends Piece {

	/**
	 * Constructor for Queen class
	 *  @param value value of Queen 
	 * 	@param color color of Queen 
	 * 
	 */
	public Queen(String value, int color) {
		super(value, color);
		
	}
	
	/**Checks legal move of Queen 
	 * @param curr Queen Current position 
	 * @param newpos position Queen is moving 
	 * @param color color of Queen 
	 * @param enpass for pawn, not used
	 */
	public boolean LegalMove(String curr, String newpos, int color, boolean enpass) {
		
		int currRow = curr.charAt(1) - '0';
		int newRow = newpos.charAt(1) - '0';
		char currCol = curr.charAt(0);
		char newCol = newpos.charAt(0);
		
		if((currRow == newRow)|| (currCol ==newCol)|| (Math.abs(currCol-newCol) == Math.abs(currRow-newRow))){ 
			Piece newspot = chess.board.get(newpos);
			//Can't capture same color piece
			
			if(clearPath(curr,newpos)) {
			if((isOccupied(newpos)== true) &&(newspot.getColor() == color) ) {

					return false;
				
			}else {
				return true;
			}
		}
		}
		return false; 
		


	}
	
	/**checks for clear path for Queen 
	 * @param curr current position of Queen 
	 * @param newpos position Queen is moving to 
	 * @return if path is clear 
	 * 
	 */
	public boolean clearPath(String curr, String newpos) {
		int currRow = curr.charAt(1) - '0';
		int newRow = newpos.charAt(1) - '0';
		char currCol = curr.charAt(0);
		char newCol = newpos.charAt(0);
		
		if((Math.abs(currCol - newCol)) == (Math.abs(currRow -newRow )) && !(curr.equals(newpos))) {
			return BishopclearPath(curr,newpos);
		}
		
		if((currRow == newRow)|| (currCol ==newCol)) {
			return RookclearPath(curr,newpos);
		}
		return false;
	}
	
	/** Rook's clear path from rook class. Helper for Queen 
	 * @param curr Queen Current position 
	 * @param newpos position Queen is moving 
	 * @return if lateral and vertical moves are clear 
	 */
	public boolean RookclearPath(String curr, String newpos) {
		int i;
		int currRow = curr.charAt(1) - '0';
		int newRow = newpos.charAt(1) - '0';
		char currCol = curr.charAt(0);
		char newCol = newpos.charAt(0);
		
		if (currRow == newRow) { //Moving in row (horizontal)
			char letter;
			if(currCol < newCol) { 
				for (letter = (char)(currCol+1) ; letter < newCol ; letter++) {
					if(!(isClearHelper(letter, currRow))) {
						return false;
					}
				}
			}
			else { 
				for (letter = (char)(newCol+1) ; letter < currCol ; letter++) {
					if(!(isClearHelper(letter, currRow))) {
						return false;
					}
				}
			}
		}else if(currCol == newCol) { //Moving in column (vertical)
			if(currRow < newRow) { //Moving up 
				for (i = currRow+1 ; i < newRow ; i++) {
					//System.out.println("1: "+ i);
					if(!(isClearHelper(currCol, i))) {
						//System.out.println("1: problem "+ i);
					return false;
					}
				}
			} else {  //Moving Down 
				for (i = newRow+1 ; i < currRow ; i++) {
					if(!(isClearHelper(currCol, i))) {
						//System.out.println("2: "+ i);
					return false;
					}
				}
			}
		}
			
			return true;
		}

	
	/**
	 * Bishop's Clear path 
	 * @param curr current position of queen 
	 * @param newpos new position queen is moving to 
	 * @return is diagonal path is clear 
	 */
	public boolean BishopclearPath(String curr, String newpos) {
		List<String> validspots=validSpots(curr, newpos);
		
		for (String str:validspots) {
			if((isOccupied(str))) { 
				return false;
			}
		}
		return true;
	}
	
	/**Helper method for clear path
	 * @param curr the Bishop's current position 
	 * @param newpos the Bishop's new position 
	 * @return the valid spots for the bishop
	 */
public static List<String> validSpots(String curr, String newpos) {
		
		
		List<String> spots = new ArrayList<String>();
		
		int currCol = (int)(curr.charAt(0)); 
		int newCol = (int)(newpos.charAt(0));
		int currRow = curr.charAt(1) - '0'; 
		int newRow = newpos.charAt(1) - '0';
	
		
		int colDist = Math.abs(newCol - currCol);
		int rowDist = Math.abs(newRow - currRow);
		
		int colDir = colDist/(newCol - currCol);
		//System.out.println("colDir:" + colDir);
		int rowDir = rowDist/(newRow - currRow);
		//System.out.println("rowDir" + rowDir);
		
		for(int i = 1; i < rowDist; i++) {
			char col = (char)(currCol + i*colDir);
			int row = currRow + i*rowDir;
			String valid_col = Character.toString(col);
			String valid_row = Integer.toString(row);
			
			spots.add(valid_col + valid_row + "");
		}
		
		return spots;
	}
	
 /** 
  * Helper for  clear path 
  * @param o char for first part of position 
  * @param i int for second part of position 
  * @return if spot is clear 
  */
	public boolean isClearHelper(char o, int i) {
		String str = o + "" + i;
		
		if(!isOccupied(str)) { 
			//System.out.println("Box is empty");
			return true;
		}
		
		return false;
	}

	
	
}
