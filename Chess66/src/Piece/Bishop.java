package Piece;

import Chess66.chess;
import java.util.ArrayList;
import java.util.List;

/**
 * Bishop class, extends piece
 * @author edward esposito and Marjan Atienza
 *
 */
public class Bishop extends Piece {
/**
 * Constructor for Bishop Class
 * @param value value of Bishop
 * @param color color of Bishop 
 */
	public Bishop(String value, int color) {
		super(value, color);
		
	}
/**Overwrite of Piece LegalMove. Checks if move is legal for piece 
 * @param curr piece's current position before moving
 * @param newpos position that piece is moving to 
 * @param  color color of piece
 * @param enpass specifically for pawn, check if enpass is valid, ignored by other pieces 
 * @return if the move is legal 
 * 
 */
	public boolean LegalMove(String curr, String newpos, int color, boolean enpass) {
		if((Math.abs(curr.charAt(0) - newpos.charAt(0)) == Math.abs (curr.charAt(1) - newpos.charAt(1))) && !(curr.equals(newpos))) {
			
			Piece newspot = chess.board.get(newpos);
			//Can't capture same color piece
			
			if(!(clearPath(curr,newpos))) {
				//System.out.println("here");
				return false;
			}
			if((isOccupied(newpos)== true) &&(newspot.getColor() == color) ) {
				

					return false;
				
			}else{
					return true;
				}
			}
		
		return false;
	}
	
	/**Checks to see if path is clear for movement
	 * @param curr the Bishop's current position 
	 * @param newpos the Bishop's new position 
	 * @return if path is clear
	 */
	public boolean clearPath(String curr, String newpos) {
		List<String> validspots=validSpots(curr, newpos);
		
		for (String str:validspots) {
			if((isOccupied(str))) { 
				return false;
			}
		}
		return true;
	}
	
	/**Helper method for clear path
	 * @param curr the Bishop's current position 
	 * @param newpos the Bishop's new position 
	 * @return the valid spots for the bishop
	 */
	public static List<String> validSpots(String curr, String newpos) {
		
		
		List<String> spots = new ArrayList<String>();
		
		int currCol = (int)(curr.charAt(0)); 
		int newCol = (int)(newpos.charAt(0));
		int currRow = curr.charAt(1) - '0'; 
		int newRow = newpos.charAt(1) - '0';
	
		
		int colDist = Math.abs(newCol - currCol);
		int rowDist = Math.abs(newRow - currRow);
		
		int colDir = colDist/(newCol - currCol);
		//System.out.println("colDir:" + colDir);
		int rowDir = rowDist/(newRow - currRow);
		//System.out.println("rowDir" + rowDir);
		
		for(int i = 1; i < rowDist; i++) {
			char col = (char)(currCol + i*colDir);
			int row = currRow + i*rowDir;
			String valid_col = Character.toString(col);
			String valid_row = Integer.toString(row);
			
			spots.add(valid_col + valid_row + "");
		}
		
		return spots;
	}
	
}

