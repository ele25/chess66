package Piece;

/**
 * Empty Spot  class extends Piece
 * @author edward esposito and Marjan Atienza
 *
 */
public class Empty extends Piece {
	/** 
	 * Constructor for Empty Spot
	 * @param value value of empty spot
	 * @param color color of empty spot 
	 */
	public Empty(String value, int color) {
		super(value, color);
	}
	
	/**Piece LegalMove. Checks if move is legal for Empty Spot. Can not move empty spot
	 * @param curr Empty does not move
	 * @param newpos  Empty does not move
	 * @param  color Empty does not move
	 * @param enpass All moves are Illegal 
	 * @return Move is always false for empty spot
	 * 
	 */
	public boolean LegalMove(String curr, String newpos, int color, boolean enpass) {
		return false; 
	}

}




